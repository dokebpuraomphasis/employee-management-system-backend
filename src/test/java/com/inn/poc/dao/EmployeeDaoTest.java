package com.inn.poc.dao;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.inn.poc.model.Employee;



@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class EmployeeDaoTest {


	@Autowired
	private EmployeeDao employeeDao;
	
	@Test
	public void whenFindByid_thenReturnEmployee() {
		
	    // given
	    Employee rohit = new Employee(1,"Rohit","rohit23");
//	    entityManager.persist(rohit);
//	    entityManager.flush();

	    // when
	    Employee found =employeeDao.findById(rohit.getEmpId()).get();
	    	

	    // then
	    Assertions.assertNotNull(found);
//	    assertThat(found.getEmpName())
//	      .isEqualTo(rohit.getEmpName());
	}
	
	 @Test
	    public void  testSaveEmployee(){

	        // given - setup or precondition
		  Employee rohit = new Employee(1,"Rohit","rohit23");

	        // when - action or the testing
		  Employee emp=this.employeeDao.save(rohit);
	       
	        // then - very output
	        Assertions.assertNotNull(emp);
	        Assertions.assertNotNull(emp.getEmpId());

	    }
	 
	 @Test
	 public void  testUpadateEmployee() {
		 // given - setup or precondition
		  Employee rohit = new Employee(1,"Rohit","rohit23");

	        // when - action or the testing
		  
		  Employee emp=this.employeeDao.save(rohit);
	       emp.setPassword("rohit001");
	       
	       Employee empUpdated=this.employeeDao.save(emp);
	        // then - very output
	        Assertions.assertNotNull(empUpdated);
	        Assertions.assertNotNull(empUpdated.getPassword());
	 }
	 
	 @Test
	 public void  testGetEmployee() {
		 // given - setup or precondition
		  Employee rohit = new Employee(1,"Rohit","rohit23");
		  Employee akash=new Employee(2,"Akash","akash90");
	        // when - action or the testing
		  
		 this.employeeDao.save(rohit);
		  this.employeeDao.save(akash);
	       List<Employee>empList=employeeDao.findAll();
	       
	        // then - very output
	        Assertions.assertNotNull(empList);
	 }
	 
	 
}
