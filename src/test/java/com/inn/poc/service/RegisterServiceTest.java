package com.inn.poc.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.inn.poc.dao.EmployeeDao;
import com.inn.poc.model.Employee;
import com.inn.poc.serviceimpl.RegisterServiceImpl;

@ExtendWith(MockitoExtension.class)
class RegisterServiceTest {
	
	@Mock
    private EmployeeDao  employeeDao;

    @InjectMocks
    private RegisterServiceImpl registerServiceImpl;

 
	@DisplayName("JUnit test for saveEmployee Or Signup  method")
    @Test
    public void tessignup() {
    	Employee e1= new Employee(1,"Rahul","rahul23");
    	Map<String,String> map=new HashMap<>();
    	 map.put("empName","Rahul" );
    	 map.put("password", "rahul23");
    	this.registerServiceImpl.Signup(map);	
    	verify(this.employeeDao, times(1)).save(e1);
    }
}
