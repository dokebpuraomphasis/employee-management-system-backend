package com.inn.poc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.inn.poc.dao.EmployeeDao;
import com.inn.poc.model.Employee;
import com.inn.poc.serviceimpl.LoginServiceImpl;

@ExtendWith(MockitoExtension.class)
class LoginServiceTest {
	@Mock
    private EmployeeDao  employeeDao;

    @InjectMocks
    private  LoginServiceImpl   loginServiceImpl ;

    @DisplayName("JUnit test for login  method")
    @Test
    public void testLogin() {
    	Employee e1= new Employee(1,"Rahul","rahul23");
    	
    	given(employeeDao.findByEmpName("Rahul")).willReturn(e1);
    	
    	Map<String,String> map=new HashMap<>();
    	map.put("empName", e1.getEmpName());
    	map.put("password", e1.getPassword());
    	boolean success=this.loginServiceImpl.login(map);	
    	assertEquals(true, success);
    }
}
