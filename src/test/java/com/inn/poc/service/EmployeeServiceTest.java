package com.inn.poc.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.inn.poc.dao.EmployeeDao;
import com.inn.poc.model.Employee;
import com.inn.poc.serviceimpl.EmployeeServiceImpl;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceTest {

	@Mock
    private EmployeeDao  employeeDao;

    @InjectMocks
    private EmployeeServiceImpl employeeService;

    private Optional<Employee> employee;

    @BeforeEach
    public void setup(){
        employee = Optional.of(new Employee(1,"Rohit","rohit23"));
               
    }
    
    @DisplayName("JUnit test for getEmployee method")
    @Test
    public void testgetEmployeeById() {
    	when(this.employeeDao.findById(1)).thenReturn(employee);

		Employee emp = this.employeeService.getEmployee(1);

		assertEquals("Rohit", emp.getEmpName());
		
    }

    
    
    @DisplayName("JUnit test for getAllEmployee method")
    @Test
    public void testgetAllEmployee() {
    	List<Employee> empList= new ArrayList<>();
    	
    	Employee e1= new Employee(1,"Rahul","rahul23");
    	Employee e2= new Employee(2,"amol","amol23");
    	
    	empList.add(e1);
    	empList.add(e2);
    	
    	
    	when(this.employeeDao.findAll()).thenReturn(empList);

		//test
		List<Employee> employeeList = this.employeeService.getAllEmployee();

		assertEquals(2, employeeList.size());
		verify(this.employeeDao, times(1)).findAll();
    }
    
    
    
    @DisplayName("JUnit test for updateEmployee method")
    @Test
    public void testUpdateemployee() {
    	
    	Employee emp=new Employee(1,"Rahul","rahul23");
    	
    	given(employeeDao.save(emp)).willReturn(emp);
    	
    	emp.setPassword("rahul11");
    	Employee e=	this.employeeService.updateEmployee(emp,1);
    	
    	 assertThat(e.getPassword()).isEqualTo("rahul11");
      
    	
    }
    
    @DisplayName("JUnit test for deleteEmployee method")
    @Test
    public void testdeleteEmployee() {
    	Employee e=new Employee(1,"Rahul","rahul23");
    	if(e!=null)
		employeeService.deleteEmployee(1);
    // then - verify the output
        verify(employeeDao, times(1)).deleteById(1);
    }
    
}



