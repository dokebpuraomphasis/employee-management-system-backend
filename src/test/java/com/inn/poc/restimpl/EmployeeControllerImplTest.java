package com.inn.poc.restimpl;

import java.awt.PageAttributes.MediaType;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.BDDMockito.given;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.inn.poc.model.Employee;
import com.inn.poc.service.EmployeeService;

@RunWith(SpringRunner.class)
@WebMvcTest(EmployeeControllerImpl.class)
class EmployeeControllerImplTest {

	@Autowired
    private MockMvc mvc;

    @MockBean
    private EmployeeService service;
    
    @Test
    public void testGetAllEmployes()  throws Exception {
    	 Employee rohit = new Employee(1,"Rohit","rohit23");

         List<Employee> allEmployees = Arrays.asList(rohit);

         given(service.getAllEmployee()).willReturn(allEmployees);

         mvc.perform(get("/employee/all")
           )
           .andExpect(status().isOk());
    }
   
    @Test
    public void testGetEmployee() throws Exception {
    	 Employee rohit = new Employee(1,"Rohit","rohit23");
    	   given(service.getEmployee(rohit.getEmpId())).willReturn(rohit);
    	   
    	   mvc.perform(get("/employee/get/1")
    	           )      .andExpect(status().isOk());
    }
    
    
    @Test
    public void testUpdateEmployee()  throws Exception {
    	 Employee rohit = new Employee(1,"Rohit","rohit23");
  	   given(service.getEmployee(rohit.getEmpId())).willReturn(rohit);
  	   rohit.setPassword("rohit01");
  	   given(service.updateEmployee(rohit,1)).willReturn(rohit);
  	   
  	 mvc.perform(put("/employee/employee")
	           )      .andExpect(status().isOk());
    }
    
    @Test
    public void testDeleteEmployee()  throws Exception {
    	 Employee rohit = new Employee(1,"Rohit","rohit23");
  	   given(service.deleteEmployee(rohit.getEmpId())).willReturn("Employee Deleted Succesfully..!");
  	 
  	 mvc.perform(delete("/employee/delete/1")
	           )      .andExpect(status().isOk());
    }
}
