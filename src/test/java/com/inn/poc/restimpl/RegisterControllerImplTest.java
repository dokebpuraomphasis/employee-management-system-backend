package com.inn.poc.restimpl;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.inn.poc.model.Employee;
import com.inn.poc.service.RegisterService;

@RunWith(SpringRunner.class)
@WebMvcTest( RegisterControllerImpl.class)
class RegisterControllerImplTest {
	
	@Autowired
    private MockMvc mvc;
	
	@MockBean
	private RegisterService service;
	
	@Test
    public void testLogin()  throws Exception {
    	 Employee rohit = new Employee(1,"Rohit","rohit23");
    	 Map<String,String> map=new HashMap<>();
    	 map.put("empName","Rohit" );
    	 map.put("password", "rohit23");
         given(service.Signup(map)).willReturn(rohit);

         mvc.perform(post("/auth/signup")
           )
           .andExpect(status().isOk());
    }


}
