package com.inn.poc.service;

import java.util.Map;

public interface LoginService {

	public boolean login( Map<String,String> requestMap);
}
