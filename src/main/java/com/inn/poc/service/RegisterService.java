package com.inn.poc.service;


import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.inn.poc.model.Employee;

public interface RegisterService {
	public Employee Signup(Map<String,String> requestMap);
}
