package com.inn.poc.service;

import java.io.IOException;

import org.springframework.http.RequestEntity.BodyBuilder;
import org.springframework.web.multipart.MultipartFile;

import com.inn.poc.model.ImageModel;

public interface ImageService {

	
	public BodyBuilder uplaodImage(MultipartFile file) throws IOException ;
	
	public ImageModel getImage( String imageName) throws IOException;
}
