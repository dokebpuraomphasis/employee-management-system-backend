package com.inn.poc.service;

import java.util.List;

import com.inn.poc.model.Employee;

public interface EmployeeService {

public	List<Employee> getAllEmployee();

public	Employee getEmployee(Integer id);
	
public	String deleteEmployee( Integer id);

public	Employee updateEmployee(Employee emp ,Integer  id);

}
