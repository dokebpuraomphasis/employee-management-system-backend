package com.inn.poc.constatnts;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class EmployeeUtil {

	private EmployeeUtil(){
		
	}
	
	public static ResponseEntity<String> getResponseEntity(String responseMessage,HttpStatus  httpStatus){
		return new ResponseEntity<String>("{\"message\":\""+responseMessage+"\"}",httpStatus);
	}
}
