package com.inn.poc.model;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="Employee")
public class Employee implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Employee_Id")
	private Integer empId ;
	
	@Column(name="employee_Name")
	private String empName;
	
	@Column(name="employee_password")
	private String password;
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="role_Id")
	private Role role;
	
	@OneToOne(fetch = FetchType.LAZY)
	 @JoinColumn(name="dept_Id")
	private Department department;
	
	@Column(name="Image_Name")
	private String  imageName;

	

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	

	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	

	public Employee() {
		super();
	}



	public Employee(Integer empId, String empName, String password) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.password = password;
	}

	public Employee(Integer empId, String empName, String password, Role role, Department department,
			String imageName) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.password = password;
		this.role = role;
		this.department = department;
		this.imageName = imageName;
	}

	

	
	
	
	
}
