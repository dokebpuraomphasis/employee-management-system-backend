package com.inn.poc.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inn.poc.model.ImageModel;

public interface ImageDao extends JpaRepository<ImageModel ,  Long> {

	Optional<ImageModel> findByName(String imageName);

	void deleteByName(String name);

}
