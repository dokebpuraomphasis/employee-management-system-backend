package com.inn.poc.serviceimpl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.inn.poc.dao.EmployeeDao;
import com.inn.poc.model.Employee;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private EmployeeDao employeeDao;

    @Override
    
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Employee emp = this.employeeDao.findByEmpName(username);

        Set < GrantedAuthority > grantedAuthorities = new HashSet < > ();
        grantedAuthorities.add(new SimpleGrantedAuthority("USER"));
        grantedAuthorities.add(new SimpleGrantedAuthority("ADMIN"));
       
        return new org.springframework.security.core.userdetails.User(emp.getEmpName(), emp.getPassword(),
            grantedAuthorities);
    }
}
