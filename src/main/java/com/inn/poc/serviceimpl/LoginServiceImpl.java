package com.inn.poc.serviceimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.inn.poc.dao.EmployeeDao;
import com.inn.poc.model.Employee;
import com.inn.poc.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private EmployeeDao employeeDao;
	
	@Override
	public boolean login(Map<String, String> requestMap) {
		
		 BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	        
		System.out.println(requestMap);
		
		String empName =requestMap.get("empName");
		String employeePassword= requestMap.get("password");
		
		System.out.println(empName +" : "+employeePassword);
		
		Employee emp=this.employeeDao.findByEmpName( empName); 
		System.out.println(passwordEncoder.matches( employeePassword,emp.getPassword()));
		
			return passwordEncoder.matches( employeePassword,emp.getPassword());
		}	
	}

