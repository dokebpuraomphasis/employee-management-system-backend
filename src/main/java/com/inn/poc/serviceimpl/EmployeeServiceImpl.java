package com.inn.poc.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inn.poc.dao.EmployeeDao;
import com.inn.poc.model.Employee;
import com.inn.poc.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;
	
	
	@Override
	public List<Employee> getAllEmployee() {
		return this.employeeDao.findAll();
	}

	@Override
	public Employee getEmployee(Integer id) {
		return this.employeeDao.findById(id).get();
	}

	@Override
	public String deleteEmployee(Integer id) {
		this.employeeDao.deleteById(id);
		return "Employee Deleted SuccessFully";
	}

	@Override
	public Employee updateEmployee(Employee emp,Integer id) {
		if(id== emp.getEmpId()) {
			return this.employeeDao.save(emp);
		}
		return null;
	}

}
