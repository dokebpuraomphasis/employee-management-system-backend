package com.inn.poc.serviceimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.inn.poc.dao.EmployeeDao;
import com.inn.poc.model.Department;
import com.inn.poc.model.Employee;
import com.inn.poc.model.Role;
import com.inn.poc.service.ImageService;
import com.inn.poc.service.RegisterService;

@Service
public class RegisterServiceImpl implements RegisterService {

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired 
	private ImageService imageService;
	
	@Override
	public Employee Signup(Map<String,String> requestMap) {
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		String empName =requestMap.get("empName");
		String employeePassword= requestMap.get("password");
		String imageName= requestMap.get("name");
		System.out.println(requestMap);
	
		System.out.println( empName+ " : "+employeePassword );
		Employee emp=new Employee();
		Role role = new Role();
		
		Department dept = new Department();
		role.setRoleid(2);
		dept.setDeptId(2);
		emp.setRole(role);
		emp.setDepartment(dept);
		emp.setEmpName(empName);
		emp.setPassword(passwordEncoder.encode(employeePassword));
		emp.setImageName(imageName);
			return this.employeeDao.save(emp);		
	}

}
