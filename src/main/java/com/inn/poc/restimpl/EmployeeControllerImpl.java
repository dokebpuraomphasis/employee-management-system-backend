package com.inn.poc.restimpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.inn.poc.constatnts.EmployeeConstant;
import com.inn.poc.constatnts.EmployeeUtil;
import com.inn.poc.model.Employee;
import com.inn.poc.model.ImageModel;
import com.inn.poc.rest.EmployeController;
import com.inn.poc.service.EmployeeService;
import com.inn.poc.service.ImageService;

@RestController
@CrossOrigin("*")
public class EmployeeControllerImpl implements  EmployeController {

	@Autowired
	private EmployeeService service;
	
	@Autowired
	private ImageService imageService;
	
	@Override
	public ResponseEntity<List<Employee>> getAllEmployee() {
		try {
			List<Employee> empList=	this.service.getAllEmployee();
			return new ResponseEntity<List<Employee>>(empList,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Employee>>(new ArrayList<Employee>(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<Employee> getEmployee(Integer id) {
		try {
			Employee emp= this.service.getEmployee(id);
			return new ResponseEntity<Employee>(emp,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Employee>(new Employee(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		}
		

	@Override
	public ResponseEntity<?> updateEmployee(Employee emp,Integer id) {
		try {
			this.service.updateEmployee(emp,id);
			return EmployeeUtil.getResponseEntity(EmployeeConstant.EMPLOYEE_UPDATE, HttpStatus.OK) ;
		}catch(Exception e) {
			e.printStackTrace();
			return EmployeeUtil.getResponseEntity(EmployeeConstant.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
	}
	

	@Override
	public ResponseEntity<String> deleteEmployee(Integer id) {
		try {
			 this.service.deleteEmployee(id);
			return EmployeeUtil.getResponseEntity(EmployeeConstant.EMPLOYEE_DELETE, HttpStatus.OK) ;
		}catch(Exception e) {
			e.printStackTrace();
			return EmployeeUtil.getResponseEntity(EmployeeConstant.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
	}

	@Override
	public BodyBuilder uplaodImage(MultipartFile file) throws IOException {
		try {
			return this.imageService.uplaodImage(file);
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	
	}

	@Override
	public ImageModel getImage(String imageName) throws IOException {
		try {
			System.out.println(imageName);
			return this.imageService.getImage(imageName);
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
		}
	}
	
	


	
	
	

	


