package com.inn.poc.restimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.inn.poc.constatnts.EmployeeConstant;
import com.inn.poc.constatnts.EmployeeUtil;
import com.inn.poc.rest.RegisterController;
import com.inn.poc.service.RegisterService;

@RestController
@CrossOrigin("*")
public class RegisterControllerImpl implements RegisterController {

	@Autowired
	private RegisterService service;
	
	@Override
	public ResponseEntity<?> signup(Map<String,String> requestMap) {
		try {
			this.service.Signup(requestMap);
			return EmployeeUtil.getResponseEntity(EmployeeConstant.EMPLOYEE_SAVED, HttpStatus.OK) ;
	}catch(Exception e) {
		e.printStackTrace();
	}
	return EmployeeUtil.getResponseEntity(EmployeeConstant.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR) ;
}
	}

	


