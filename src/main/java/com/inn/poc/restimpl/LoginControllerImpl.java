package com.inn.poc.restimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.inn.poc.constatnts.EmployeeConstant;
import com.inn.poc.constatnts.EmployeeUtil;
import com.inn.poc.rest.LoginController;
import com.inn.poc.service.LoginService;
@RestController
@CrossOrigin("*")
public class LoginControllerImpl implements LoginController {

	@Autowired
	private LoginService service;
	
	@Override
	public ResponseEntity<?> login(Map<String, String> requestMap) {
		try {
			boolean success=	this.service.login(requestMap);
			if(success) {
				return EmployeeUtil.getResponseEntity(EmployeeConstant.LOGIN_SUCCESS, HttpStatus.ACCEPTED) ;
			}else {
				return EmployeeUtil.getResponseEntity(EmployeeConstant.EMPLOYEE_NOT_EXIST, HttpStatus.INTERNAL_SERVER_ERROR) ;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			return EmployeeUtil.getResponseEntity(EmployeeConstant.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
		
	
	}

	

}
