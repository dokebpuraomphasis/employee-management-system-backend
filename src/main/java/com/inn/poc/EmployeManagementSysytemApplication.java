package com.inn.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeManagementSysytemApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeManagementSysytemApplication.class, args);
	}
	
}
