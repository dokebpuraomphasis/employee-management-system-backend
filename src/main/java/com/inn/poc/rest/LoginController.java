package com.inn.poc.rest;

import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(path="/auth")
@CrossOrigin("*")
public interface LoginController {

	@PostMapping(path="/login")
	public ResponseEntity<?> login(@RequestBody (required=true)  Map<String,String> requestMap);
}
