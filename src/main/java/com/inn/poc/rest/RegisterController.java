package com.inn.poc.rest;

import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping(path="/auth")
@CrossOrigin("*")
public interface RegisterController {

	@PostMapping(path="/signup")
	public ResponseEntity<?> signup(@RequestBody (required=true) Map<String,String> requestMap);
	
}
