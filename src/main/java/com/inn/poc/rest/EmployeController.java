package com.inn.poc.rest;

import java.io.IOException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.http.RequestEntity.BodyBuilder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.inn.poc.model.Employee;
import com.inn.poc.model.ImageModel;

@RequestMapping(path="/employee")
@CrossOrigin("*")
public interface EmployeController {

	@GetMapping(path="/all")
	public ResponseEntity<List<Employee>> getAllEmployee();
	
	@GetMapping(path="/get/{id}")
	public  ResponseEntity<Employee>getEmployee(@PathVariable("id") Integer id);
	
	@PutMapping(path="/employee/{id}")
	public ResponseEntity <?>updateEmployee(@RequestBody (required=true)  Employee emp,@PathVariable("id") Integer id);
	
	@DeleteMapping(path="/delete/{id}")
	public ResponseEntity<String> deleteEmployee(@PathVariable("id") Integer id);
	
	@PostMapping("/upload")
	public BodyBuilder uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException ;
	
	@GetMapping(path = "/getImage/{imageName}" )
	public ImageModel getImage(@PathVariable("imageName") String imageName) throws IOException;
}
